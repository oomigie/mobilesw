.include "DP256reg.s"

; ========================================================
;	basicLCD	Include file with minimal support 
;			for LCD
;	Requires : dp256reg.asm
;
;	Hacked from Lcd_2PP.asm:     L  Parallel Lcd Driver 
;         Version:       1.0   Released 11/01/2002
;          Author:       Gary Karnes , Axiom Manufacturing
;        Compiler:       Asm12
;        Platform:       CML12S & PROJECT BOARD 
;
; ========================================================
;
; Equates
U21_N = $01           ; LCD unused pin
LCD_RW = $02          ; LCD RW select (PT1)
LCD_RS = $04          ; LCD RS select	(PT2)
LCD_EN = $08          ; LCD EN select	(PT3)
U21_EN = $80

_LCD_instruction::
	 pshd
	 pshx
	 pshy
	 tba
	 jsr LD2PP_Instruction
	 puly
	 pulx
	 puld
	 rts
_LCD_display::
 	 pshd
	 pshx
	 pshy
	 tba
	 jsr LCD2PP_Data
	 puly
	 pulx
	 puld
	 rts
	 
;-----------
_Lcd2PP_Init::
	 pshd
	 pshx
	 pshy
	 jsr Lcd2PP_Init
	 puly
	 pulx
	 puld
	 rts	

_Lcd2PP_Init2::
	 pshd
	 pshx
	 pshy
	 jsr Lcd2PP_Init2
	 puly
	 pulx
	 puld
	 rts	
	  
Lcd2PP_Init:	; Note : Use 4-bit init sequence (not 8-bit)  Page 3 LCD_spec.pdf
		; Bottom table contains sequence of instructions
		; Each row in the table represents one WRITE to the LCD instruction register (via Port P)
		;	First instruction involves only a 4-bit instruction (one WRITE)
		;	Following instructions involve 8 bit instruction, therefore
		;		2 * 4-bit writes

	; "System init"
	; Setup Port T for output
          movb #$0F,DDRT        ; setup port T
          movb #$00,PTT         ; all low
	; Disable SPI AND setup SPI1 as four output bits
	      bset  DDRP,#$0F   	; set P0-3 as outputs
          bclr  SPI1CR1,#$40  	; Disable SP by turning SPI1 off

          movb #$FE,DDRM        ; set PM1-7 as outputs
          movb #$00,PTM         ; D.P.(PM2) = Off, U7_EN(PM3)= low,
                                ; SS0*(PM4), SS1*(PM5), SS2*(PM6) = Low
                                ; Heat(PM7) = Off

          bclr    PTT,LCD_RW+LCD_RS+LCD_EN  ; select lcd commands Cs=0 En=0

          jsr      DELAY50M
          ldaa     #$02		; Set to 4-bit operation (0010)
          jsr      LCD2PP_4     ; This first instruction is only 4 bits long!!!  Rest are 8 bits.  
          jsr      DELAY50M

        ;  ldaa     #$2c		; Function Set = 001(D/L)NF** where D/L = 0(4-bit) N=1(2-lines) F=0(font=5x7 dots)
          ldaa     #$28		; Function Set = 001(D/L)NF** where D/L = 0(4-bit) N=1(2-lines) F=0(font=5x7 dots)
          jsr      LD2PP_Instruction         
          jsr      DELAY10M         

          ldaa      #$0e	; Display On/off Control = 00001DCB where D=1(display on) C=1(cursor on) B=0 (blink off)
          jsr      LD2PP_Instruction          
          jsr      DELAY10M          
                
          ldaa     #$01		; Clear display = 00000001
          jsr      LD2PP_Instruction           
          jsr      DELAY20M          
          ldaa     #$80		; DDRAM Address Set = 01xxxxxx where xxxxxx = address
          jsr      LD2PP_Instruction
          jsr      DELAY10M
		  
; Reset Lcd states to rest
         bclr    PTT,LCD_RS+LCD_RW+LCD_EN ; turn all signals off on lcd
         rts
		  
Lcd2PP_Init2:	; Note : Use 4-bit init sequence (not 8-bit)  Page 3 LCD_spec.pdf
		; Bottom table contains sequence of instructions
		; Each row in the table represents one WRITE to the LCD instruction register (via Port P)
		;	First instruction involves only a 4-bit instruction (one WRITE)
		;	Following instructions involve 8 bit instruction, therefore
		;		2 * 4-bit writes

	; "System init"
	; Setup Port T for output
          movb #$0F,DDRT        ; setup port T
          movb #$00,PTT         ; all low
	; Disable SPI AND setup SPI1 as four output bits
	      bset  DDRP,#$0F   	; set P0-3 as outputs
          bclr  SPI1CR1,#$FF  	; Disable SP by turning SPI1 off from $40 to $FF

          movb #$FE,DDRM        ; set PM1-7 as outputs
          movb #$00,PTM         ; D.P.(PM2) = Off, U7_EN(PM3)= low,
                                ; SS0*(PM4), SS1*(PM5), SS2*(PM6) = Low
                                ; Heat(PM7) = Off
								
		; Reset Lcd states to rest
        ;bclr    PTT,LCD_RS+LCD_RW+LCD_EN ; turn all signals off on lcd						
          rts
		    
;
;-----------------------------------------------
; Lcd Routines
;
; Write a byte to the LCD Data Register
LCD2PP_Data:
      bset  PTT,LCD_RS     ; select lcd data buffer RS=1
      jsr   LCD_W_8        ; write byte
      rts

; Write a byte to the LCD Instruction Register (leaves LCD in Data mode)
LD2PP_Instruction:
        bclr   PTT,LCD_RS        ; select lcd command buffer
        jsr    LCD_W_8           ; wait
        bset   PTT,LCD_RS        ; select data buffer
        rts

LCD2PP_4:			; Destroys a and b
	 bset  	PTS,#U21_EN	; set U21_EN high so that latch becomes transparent
         jsr      DELAY1MS      ; delay     
         ldab     PTP              ; Port P
         andb     #$f0             ; get only bits 4 - 7
         anda     #$0f             ; get data
         aba
         staa     PTP              ; save data 
	; For LCD's write cycle, Enable must pulse high and then low (for specified time)
         bclr     PTT,LCD_EN       ; enable low
         jsr      DELAY1MS         ; delay for LCD
         bset     PTT,LCD_EN       ; latch data
         jsr      DELAY1MS         ; delay for LCD 
         bclr     PTT,LCD_EN           ; enable low
         jsr      DELAY1MS
	 bclr  PTS,#U21_EN    ; set U21_EN low to isolate LCD from parallel control (outputs are latched)
         rts
;
;
; Lcd Write 8 bit Data , lower 4 bits first in acc A   (Destroys A)
LCD_W_8:					
         psha                     ; save a 
         lsra                     ; shift upper 4 bits to lower
         lsra
         lsra
         lsra
         jsr      LCD2PP_4        ; write upper 4 bits to lcd
         pula
         jsr      LCD2PP_4         ; write lower 4 bits to lcd
         rts
					

;
; Delay routines
;
;
; Generate a 50 ms delay
DELAY50M:
          pshx
          ldx  #49998      ; delay 50,000 usecs,
          jsr  DELML01     ; call usec delay
          pulx
          rts
;
;
; Generate a 20 ms delay
DELAY20M:
          bsr  DELAY10M
          bsr  DELAY10M 
          rts
;
; Generate a 10 ms delay
DELAY10M:                            ; jsr=4cyles
          pshx             ; 2 cycles ,save x
          ldx  #9998       ; 2 cycles,delay 9998 usec + 2 for this routine
          jsr  DELML01     ; call usec delay, this delay offset in sub
          pulx             ; 3 cycles restore x
          rts              ; 5 cycles
;
;
; Generate a 1 ms delay
DELAY1MS:
                           ; jsr=4cyles
          pshx             ; 2 cycles ,save x
          ldx  #998       ; 2 cycles,delay 9998 usec + 2 for this routine
          jsr  DELML01     ; call usec delay, this delay offset in sub
          pulx             ; 3 cycles restore x
          rts              ; 5 cycles


;
; 8 cycles = 1 usec e = 8mhz
DELML01:
          nop              ; 1 cycle
          nop              ; 1 cycle
          nop              ; 1 cycle
          nop              ; 1 cycle
          dex              ; 1 cycle 
          bne   DELML01    ; 3 cycles
          rts              ; 5 cycles
		  
		  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;recoup assign 
	
FALSE =  0    ;;just for readability
DDRK = $33	;port K's data direction register
PORTK = $32	; port K's data register
PTT = $240 ;portT data register
DDRT = $242 ;portT direction register
PTM = $250 ;portM data register





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; void init(void) ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Initializes all hardware related to 7segment display and LEDs e.g DDR 
_init:: 

	pshd    ;;pushing first argument onto stack  
	pshx    ;;push base pointer
	tfr sp,x   ;;store the current position of the stack as my reference 
	
	;leas -6,sp   ;allocate local variables on stack


	movb #$0F,DDRK		;setup port K with bits 0 ...3 as output pins (1 = output)	

	;movb #$00, PORTK      ; all low = all off


	;bset PORTK,#$01  ;; simply turn on LED 1 for now

 ;;;;;;;;;;delay(void) for approximately 1 second;;;;
	
	
		

   ;;;;;;;;;;;end delays;;;;;;;

;;;;turn off all leds afterwards
	;movb #$00,PORTK
	
	tfr x,sp  ;;;deallocate local variables
	pulx	   ;;Restore X
	leas 2,sp   ; remove argument zero from the stack
	;ldd #$3100
	;std Result_str
	rts


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;











;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;void delay(void);;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
_delay::
	pshd    ;;pushing first argument onto stack  
	pshx    ;;push base pointer
	tfr sp,x   ;;store the current position of the stack as my reference 
	
	;leas -6,sp   ;allocate local variables on stack

   ;;;;;;;;;;delay for approximately 1 second;;;;
	ldy #$FFFF

loop: 	psha
      	pula
      	psha
      	pula
		psha
      	pula
      	psha
      	pula
		psha
      	pula
      	psha
      	pula
      	dbne y, loop	
   ;;;;;;;;;;;end delays;;;;;;;

	tfr x,sp  ;;;deallocate local variables
	pulx	   ;;Restore X
	leas 2,sp   ; remove argument zero from the stack
	;ldd #$3100
	;std Result_str
	rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;












;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;void setLED( unsigned byte index, boolean on );;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

_setLED::
	pshd    ;;pushing first argument onto stack  
	pshx    ;;push base pointer
	tfr sp,x   ;;store the current position of the stack as my reference 
	
	;leas -6,sp   ;allocate local variables on stack

	;movb #$00, PORTK ;;;;turn off all LEDs

	
	ldy 6,x   ;;;load register x with the "second parameter" which is our boolean on.
	cpy #0     ;;;check if our boolean is false that is off


	bne turnON_LED     ;;;turn on the LED

	;bra turnOFF_LED    ;;;;else turn off LED
        bra turnOFF_LED

	;bra done_setLED
	;bra turnOFF_LED   ;;;;else it turns off LED 	

	



done_setLED:	
	tfr x,sp  ;;;deallocate local variables
	pulx	   ;;Restore X
	leas 2,sp   ; remove argument zero from the stack
	rts


;--------------turn on LEDs sub routines -------------------------------------------------
turnON_LED:
	
	;;;here we determine which LED do we want to turn on from the index parameter;;;;
	ldy 2,x   ;;load the 1st argument passed from setLED(unsigned byte index, boolean on) onto register y
	cpy #0     ;; this value is the index of the LED to turned on
	beq turnON_redLED   ;;;red led is on index 0, turn it on
	
	;;;;cpy may change the N,Z,V,C so we load the value in register y everytime we need to compare
	ldy 2,x   ;;load the 1st argument passed from setLED(unsigned byte index, boolean on) onto register y
	cpy #1     ;; this value is the index of the LED to turned on
	beq turnON_whiteLED   ;;;;;;red led is at index 1, turn it on
	
	;;;;cpy operation above may have changed the N,Z,V,C so we load the value in register y everytime we need to compare
        
	ldy 2,x   ;;load the 1st argument passed from setLED(unsigned byte index, boolean on) onto register y
	cpy #2     ;; this value is the index of the LED to turned on
	beq turnON_yellowLED   ;;;;;;yellow led is at index 2, turn it on
	
	;;;;cpy operation above may have changed the N,Z,V,C so we load the value in register y everytime we need to compare

	ldy 2,x   ;;load the 1st argument passed from setLED(unsigned byte index, boolean on) onto register y
	cpy #3     ;; this value is the index of the LED to turned on
	beq turnON_greenLED   ;;;;;;green led is at index 3, turn it on
	

	

	bra done_setLED   ;;;;this will branch to done_setLED subroutine which will return from the _setLED subroutine  



turnON_redLED:
	;movb #$0F,DDRK
	bset PORTK,#$01   ;;;turn on red led
	bra done_setLED   ;;;;this will branch to done_setLED subroutine which will return from the _setLED subroutine  

turnON_whiteLED:
	;movb #$0F,DDRK
	bset PORTK,#$02   ;;;turn on white led
	bra done_setLED   ;;;;this will branch to done_setLED subroutine which will return from the _setLED subroutine  

turnON_yellowLED:
	;movb #$0F,DDRK
	bset PORTK,#$04   ;;;turn on yellow led
	bra done_setLED   ;;;;this will branch to done_setLED subroutine which will return from the _setLED subroutine  

turnON_greenLED:
	;movb #$0F,DDRK
	bset PORTK,#$08   ;;;turn on green led
	bra done_setLED   ;;;;this will branch to done_setLED subroutine which will return from the _setLED subroutine  

;-------------------------------------------------------------------



;------------turn off leds sub routines-----------------------------

turnOFF_LED:
	;;;here we determine which LED do we want to turn off from the index parameter;;;;
	ldy 2,x   ;;load the 1st argument passed from setLED(unsigned byte index, boolean on) onto register y
	cpy #0     ;; this value is the index of the LED to turned off
	beq turnOFF_redLED   ;;;red led is on index 0, turn it off

	;;;;cpy may change the N,Z,V,C so we load the value in register y everytime we need to compare
	ldy 2,x   ;;load the 1st argument passed from setLED(unsigned byte index, boolean on) onto register y
	cpy #1     ;; this value is the index of the LED to turned off
	beq turnOFF_whiteLED   ;;;;;;red led is at index 1, turn it off

	
	;;;;cpy may change the N,Z,V,C so we load the value in register y everytime we need to compare
	ldy 2,x   ;;load the 1st argument passed from setLED(unsigned byte index, boolean on) onto register y
	cpy #2     ;; this value is the index of the LED to turned off
	beq turnOFF_yellowLED   ;;;;;;red led is at index 2, turn it off

	
	;;;;cpy may change the N,Z,V,C so we load the value in register y everytime we need to compare
	ldy 2,x   ;;load the 1st argument passed from setLED(unsigned byte index, boolean on) onto register y
	cpy #3     ;; this value is the index of the LED to be turned off
	beq turnOFF_greenLED   ;;;;;;red led is at index 3, turn it off

	
turnOFF_redLED:
	bclr PORTK,#$01   ;;;turn off red led
	bra done_setLED   ;;;;this will branch to done_setLED subroutine which will return from the _setLED subroutine  


turnOFF_whiteLED:
	bclr PORTK,#$02   ;;;turn off white led
	bra done_setLED   ;;;;this will branch to done_setLED subroutine which will return from the _setLED subroutine  

turnOFF_yellowLED:
	bclr PORTK,#$04   ;;;turn off yellow led
	bra done_setLED   ;;;;this will branch to done_setLED subroutine which will return from the _setLED subroutine  

turnOFF_greenLED:
	bclr PORTK,#$08   ;;;turn off green led
	bra done_setLED   ;;;;this will branch to done_setLED subroutine which will return from the _setLED subroutine  



;--------------------------------------------------------------------


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;void set7Segment( char number, boolean on ) Displays a number on the 7 Segment Display

_set7Segment::	
	pshd    ;;pushing first argument onto stack  
	pshx    ;;push base pointer
	tfr sp,x   ;;store the current position of the stack as my reference 

	bset PTM,#$08    ; Load data into U7 set U7_EN high (PM3) 
	movb #$CF,DDRT    ; setup port T (PT0..3 for BCD) (1 = output ?)
	
	ldy 6,x  ;load the 2nd argument into y
	cpy #0	 ;check if it is zero/false
	beq done_set7Segment ;if it is false, do not write to 7 segment display
	
	;else, if the 2nd argument is true or !0, then write number(1st argument) to the 7 segment display
	
	;the lower bits of port T will be used to write the Binary Coded Decimal to the 7 segment 
	stab PTT     ;write to the 7 segment 		
	jsr _delay		 ;delay here, so the digit is visible
	
	bclr PTM,#$08    ; Sets U7_EN low (PM3) latches last digit

done_set7Segment:
	tfr x,sp
	pulx
	leas 2,sp  ;;remove only the argument zero from the top
	rts

;THE FOLLOWING ARE NOT DELIVERABLES
;#include "DP256reg.asm"  
;#include "sci0api.asm"

	
