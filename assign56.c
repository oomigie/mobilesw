/**


						**********************
						*     OSYAN MOBILE   *
						********************** 
					
                         ----READ_ME-----


This is a simple application for a mobile device (running on the Motorola HC12 microcontroller). It provides specific functions for 
different modes of operation in a mobile telecommunication device. These functions interface with the needed device components
via the Motorola HC12 ports.   

From concepts learnt in this course (SYSC 2003) we have been able to make good use of interrupts (OC,Real-time,keyboard interrupts) 
in accomplishing simple multitasking for this device. The issue of I/O devices running on different clock speeds have also been 
addressed by employing an I/O synchronization method known as periodic polling. After a certain number of interrupts have occurred 
(periods specified by the ISR), the ISR indicates to the program (using global variables/flags) that the given device can now be 
polled. 

(side-note: This is a single threaded application).  

We have also written some sub-routines in assembly such as writing to the 7segment display, displaying a character on LCD, 
LED on/off etc. These subroutines (found in 'assign56ASM.s') are being interfaced with and used as helper functions in 
the C program.

To attain a certain level of modularity, we have designed our appplication into 4 different operating modes of a mobile 
telecommunication device.  

Transitions and events in the various modes:     

note: An incoming call is simulated by pressing key 'C' in 'idle' or 'dial' modes

>>Idle Mode
	Pressing Keys 0-9: switches device to dial Mode
	Pressing Key C: switches device to the incoming call mode
	Pressing Key D: switches device to dial Mode

>>OnCall Mode:
	
	Pressing Key E: (End call) displays call duration and returns to previous mode (or previous stack frame)

>>Dial Mode:
	*The telephone number is only called/sent when 6 numeric digits have been entered* 
	Pressing Keys 0-9: digits to dial are being accepted and displayed on LCD.
	Pressing Key D: switches device back to idle mode
	Pressing Key C: switches device to the incoming call mode

>>IncomingCall Mode
	Blinks "INCOMING CALL" message on the LCD while sounding the ringtone/buzzer
	Pressing Key A: (Accepts call) switches to the onCall Mode
	Pressing Key D: (Denies call) returns to previous mode (or previous stack frame)


OUR TODO LIST
- At the start of the program,
  The device does a 'bootup' count down on the 7 segment display 
- implement an SMS feature
- add a separate bootup phase intializing all required ports 
- implement a timeout on 'Incomingcall' and 'Dial' mode
- Elimniate interrupt interference by applying a more efficient periodic polling mechanism for the keypad interrupts 
  (foreground/bacground synchronization - seperating the polling algorithm getAscii() from KISR and communicating via 
	global variables and flags).   

	AUTHORS:  OSAZUWA OMIGIE (100764733)     GITHUB profile - https://github.com/osamie
			  RYAN SEYS (100817604)      GITHUB profile - https://github.com/ryanseys

**/


#include <stdio.h>
#include <hcs12dp256.h>

//macro: false defined as 0
#define FALSE 0

//macro: true is anything that isn't 0 (not false)
#define TRUE !0

//The maximum # of counts used to create a time delay.
#define DELAY_MAX 11000    	   

 //delay macro here is just a for loop.
#define delay(); for(i = 0; i < DELAY_MAX; i++);  	

#define red_LED 0
#define white_LED 1
#define yellow_LED 2
#define green_LED 3	

//defining a boolean type and byte type
typedef int boolean;

//VARIABLES
int seconds = 0;
int minutes = 0;
boolean timeChanged = FALSE;
//END VARIABLES

typedef unsigned char byte;  //Alias unsigned char data type as byte
int temperature_limit = 20; // the temp "too high" value is initialized here. (higher or equal -> blinds closed, lower -> blinds open)
byte blindsClosed = FALSE; //initially the blinds closed flag is false
int numInterrupts = 1; // keeps track of the number of seconds that have passed.

void printLCDString(char *);

void onCallMode(void);



//RECOUP 3 VARIABLES //
char key;
boolean pressed = FALSE;

//END OF RECOUP 3 VARIABLES //


// *** This has changed from question one. ***
//QUESTION 2 FUNCTION PROTOTYPES //
void printLCD(char); 
void LCD_display(char);
void LCD_instruction(byte);
void Lcd2PP_Init(void);
void Lcd2PP_Init2(void);
extern void setLED( unsigned byte, boolean );
extern void set7Segment( char number, boolean on ) ;
// END OF QUESTION 2 FUNCTION PROTOTYPES //

// this divisor is used in conjuction with the interrupt to tell it when
// the numInterrupts should increase.
int divisor = 0; 

// QUESTION 1 VARIABLES //
int F_found = 0; //F key has not been pressed to begin.
int temperature = 20; //initial temperature is 20 degrees Celcius
boolean alarmArmed = FALSE; //alarm is disarmed to begin
boolean windowSensor = FALSE; //window is not open to begin
boolean InfraredSensor = FALSE; //presense not detected to begin
// END OF QUESTION 1 VARIABLES //

// *** This has changed from question one. ***
// QUESTION 2 VARIABLES //
byte heatingON = FALSE; //heating is initialized as OFF.
// END OF QUESTION 2 VARIABLES //

// QUESTION 1 FUNCTION PROTOTYPES //
void waiting(void);
void initRow(int);
char checkKey(int);

//void delay (int);
// END OF QUESTION 1 FUNCTION PROTOTYPES //

// PROTOTYPES HERE
void runSimulation(void);
void STEPPERClkwise(int);
void STEPPERAntiClkwise(int);
char temp = 0x00;

// RECOUP3 PROTOTYPES
char getASCII(void); // Gets key and converts to ASCII
void printLCDSame(char);
// END OF RECOUP3 PROTOTYPES

/* The Timer ISR is called every time the real time interrupt occurs! 
   In this case it counts how many times it has occurred. 
   numInterrupts is increased by 1 approximately once per second.
   The interrupt handler is used in causing a 3 second delay.
*/
//void _Timer_ISR(void) {


//keypad interrupt service routine
#pragma interrupt_handler KISR()
void KISR(void) {
	 char temp = 0x00;
	//we do not want this interrupt to be interrupted by other interrupts
	 PIEH = 0x00;   // Local disable
	 //pressed = TRUE;
	 temp = getASCII();
	 if(temp != 0x00) key = temp; //set the global flag
	 
	 PTM = PTM | 0x08;
	 PTP = 0x0F; //set scan rows
	 //set U7_EN low
	 PTM = PTM & 0xf7;
	 DDRP |= 0x0F; // bitset PP0-3 as outputs (rows)
	 //DDRH &= 0x0F; // bitclear PH4..7 as inputs (columns)
	 
	 PIEH |= 0xF0; // Local enable on columns input
	 PIFH = PIFH; // Acknowledge (all) interrupts
	 //asm("rti");
}

/*
	polls the keypad for the key that was pressed
	returns the ascii value of key pressed
*/
char getASCII(void) {
	 int i = 1;
	 char temp = 0x00;  //initialize temp to 0
	 while(temp == 0x00) 
	 {
	 	   initRow(i);  //initialize row i
		   
		   //poll the keypad to see which which key was pressed
		   temp = checkKey(i);   //store the key pressed in temp
		   i++;     
		   if(i > 4) break;  //break from the loop after checking the 4th row
	 }
	 return temp;
}

/*	prints a given string on the LCD */
void printLCDString(char string[]) {
	 int i;
	 int length = strlen(string); //get string length
	 for(i = 0; i < length; i++)  //iterating through the characters in the string
	 {
	  	   printLCD(string[i]); //print character at index i on LCD
	 }
}

//delay function
void delayU(int val) {
     int delayVal;
	 for(delayVal = 0; delayVal < val; delayVal++);
}

/*
Idle mode():
	This function displays the idle/start screen on the LCD and waits for key events
	Keys 0-9: switches device to dial Mode
	Key C: switches device to the incoming call mode
	Key D: switches device to dial Mode
*/
void idleMode(){

	 char next; //local variable for storing key pressed
	 char  k;  
	 Lcd2PP_Init();  //initialize the LCD
	 writeStartScreen(); //write start/idle screen on LCD

	 asm( "cli" ); //globally enable interrupts
	 for (;;)
	 {
	 	 //Lcd2PP_Init(); 
         
		 
	 	 while (key == 0xFF); //wait here for a key to be pressed
		 next = key; //quickly make a copy of it 
		 //Lcd2PP_Init2();
		
		if(next == 'C') 
		{
			//simulate an incoming call
		  	incomingCallMode(); //
			writeStartScreen(); //write start/idle screen on LCD
		    
		}
		else if((next == 'D') || ((next >= '0') && (next <='9') ))
		{
		  	dialMode(); 
			printf("done dialing"); //debugging: print to the terminal
			writeStartScreen();  //write start/idle screen on LCD
		}
		 key = 0xFF;
	 }

}

/*writes the idle/start screen to the LCD **/
void writeStartScreen() {
    LCD_instruction(0x01); //clear display	 
    printLCDString("<<OSYAN MOBILE>>"); 
    LCD_instruction(0xc0);
    printLCDString("Call D:out C:in");
}

void delay1second() {
	 int i;
	 TSCR1 = 0x90; // movb #$90,TSCR ; enable TCNT and fast timer
	 TIE = 0x03; // movb #$05,TMSK2 ; set prescale factor to 8 (3 = 8, 5 = 32)
	 TIOS |= 0x01; //enable OC0
	 TC0 = TCNT;
	 //by experiment it shows that the frequency is actually low
	 for(i = 0; i < 125; i++) { 
	 	   TC0 += 50000;
		   while((TFLG1 & 0x01) == 0);
	 }
}


/*
dialMode()
	Accepts keypad input from the user and displays key pressed on the LCD 
	Keys 0-9: digits to dial are being accepted and displayed on LCD.
	Key D: switches the device back to idle mode
	Key C: (Simulates and incoming call to the device) Switches the device to the incoming call mode
*/
void dialMode(){
	 int digitsEntered = 0;
 	 char phoneNumbr[5];
	 char next;
	 char  k;
	 key = 0xFF;
 
	 
	 //Lcd2PP_Init();
	 LCD_instruction(0x01); //clear display	 
 	 LCD_instruction(0x0C); //turn of cursor and blink
	 printLCDString("Dial:");
	 LCD_instruction(0xc0);
	 printLCDString("Enter phone #");
	 
	 LCD_instruction(0x86);
	 
	 asm( "cli" );
	 for (;;) {
	 	 if(digitsEntered == 5){
		   //printf("maximum digits entered!");
		   dialingNumber(phoneNumbr);
		   onCallMode();
		   return;
		   
		   
		  }
	 	 while (key == 0xFF);
		 next = key;
		 //Lcd2PP_Init2();
		
		 if((next <= '9') &&(next>='0')) {
		 		  printLCD(next);
				  phoneNumbr[digitsEntered] = next;
				  ++digitsEntered;
		}
		 
		  else if(next == 'C'){
		  	incomingCallMode();   
		    //Lcd2PP_Init2(); 
			LCD_instruction(0x01); //clear display	
			printLCDString("Dial:");
		  }
		  
		  else if(next == 'D'){
		  	   //idleMode();
			   return;
			   
		  }
		 key = 0xFF;
	 }

}




/**
incomingCall() Mode
	Blinks "INCOMING CALL" message on the LCD while sounding the ringtone/buzzer
		Key A: (Accepts the call) switches to the onCall Mode
		Key D: (Rejects the call) returns to previous mode (or previous stack frame)
**/
void incomingCallMode()
{
 //int digitsEntered = 0;
	 char next;
	 char  k;
	 //key = 0xFF;
	 //Lcd2PP_Init2();
	 LCD_instruction(0x01); //clear display	 
	 
	 key = 0xFF; //don't recogniz keys pressed before the alerts displays
	 //printLCDString("INCOMING CALL!");

	 asm( "cli" );
	 //for (;;){
	 	 
	 init();
	 setLED(green_LED,FALSE);
	 setLED(red_LED,TRUE);
		 while ( (key != 'D') && (key != 'A') ){
		   LCD_instruction(0x01); //clear display
		   printLCDString(" INCOMING CALL!");
		   LCD_instruction(0xc0);
		   printLCDString("D:Deny, A:Answer");
		   ringTone();
		   //setLED(red_LED,TRUE);
		   //delayU(10);
		   //setLED(red_LED,FALSE);
  		   delayU(300);
		 }  
		 next = key; //value of key may change in the process so store its current value
		 //Lcd2PP_Init2();
		
		 if (next == 'D' ){
		 	LCD_instruction(0x01); //clear display
			setLED(red_LED,FALSE);
		 	printLCDString("Call Rejected");
			//while(1);
			//delayU(200);
			delay1second();
			key = 0xFF;
			return;
		 } 
		 else if (next == 'A' ){
		 	//printLCDString("ON CALL");
			//setLED(red_LED,FALSE);
			//delayU(200);
			
			onCallMode();
		 }
		key = 0xFF;
}

/*
	displays the time on the LCD
*/
void printTime() {
	 printLCD(minutes / 10 + 0x30); //get the MSB and convert to ascii. then print it out.
	 printLCD(minutes % 10 + 0x30); //get LSB and convert to ascii. then print it out.
	 printLCD(':');
	 //Print out the value of the seconds onto the LCD
	 printLCD(seconds / 10 + 0x30); //get the MSB and convert to ascii. then print it out.
	 printLCD(seconds % 10 + 0x30); //get LSB and convert to ascii. then print it out.
}


/*
onCallMode()
	-precondition: A call has been accepted, 
	-displays "ON CALL" and call timer on LCD
	
	Key E: (End call) displays call duration and returns to previous mode (or previous stack frame)
*/
void onCallMode(){
	 
	 char next;
	 key = 0xFF;
	 LCD_instruction(0x01); //clear display	
	 printLCDString(" ON CALL");
	 LCD_instruction(0xc0);
	 printLCDString("E: End Call");
	 LCD_instruction(0x88);
	 printLCDString(" T:");
	 seconds = 0;
	 minutes = 0;
	 //PORTK |= (1<<6);
 	 //PORTK |= (1<<7);
	 
     //printLCDString("endCall[D]");	 
     setLED(red_LED,FALSE); //ensure red LED has been turned off
	 setLED(green_LED,TRUE); //turn on green LED to indicate call in progress
	 for(;;){
	  while(key == 0xFF) {
	  //while(timeChanged == FALSE);
		 //reinitialize ports for the keypad.
		 seconds++;
		 //printf("seconds increased!");
    	 if(seconds == 60) seconds = 0; //roll over every minute
    	 if(seconds == 0) minutes++;
    	 if(minutes == 60) minutes = 0; //roll over every hour
    	 /* print to LCD here */
    	 LCD_instruction(0x8B); //MSB of minutes
    	 printTime();
		 delay1second();
    	 //timeChanged = FALSE;
	  }
	  next = key;
	  
	  if(next == 'E') { 
	  		  key = 0xFF; 
			  LCD_instruction(0x01); //clear display	
	 		  printLCDString(" CALL ENDED! ");
			  LCD_instruction(0xc0);
			  printLCDString("Duration:");
			  printTime();
			  delay1second(); //delay
			  delay1second(); //delay
			  return; 
			  
	  } //hangup 
	  else key = 0xFF; //go back to timing
	 }
}

#pragma interrupt_handler OC_TIMER
void OC_TIMER(void) {
	 TC0 = TCNT;
}

/* Sound the buzzer on the board for one second. */
void ringTone() {
	int i; //for the delay macro
	int x; //for the for-loop
	DDRK |= (1 << 5); 
	//DDRK |=  // outputs for the buzzer set
	//PORTK |= 0x20; //turning on the buzzer bit
	PORTK |= (1 << 5); //turning on the buzzer bit

	
	//delay here
	for(x = 0; x < 12; x++) {
		  delay(); 
	}
	
//	PORTK |= 0x00;  
	PORTK &= ~(1 << 5); //turn off the buzzer afterwards
	
	DDRK &= ~(1 << 5); // outputs
}


/*
  displays "dialing on the LCD and make indications using the LED to indicate attempt to dial number"
*/
void dialingNumber(char string[]){
	 int i;
	 int w;
	 int k = 6;  //after 6 numbers have been entered
 	 int length = strlen(string);
	 //Lcd2PP_Init();
//	 while(1){
	 
 	 LCD_instruction(0x01); //clear display	 
	 printLCDString("DIALING ");
	 

	 
	 for(w = 0; w < length; w++) {
	  	   printLCD(string[w]);
	 }
	 
	 printLCDString("...");
	 
	 init(); //intializations for LED and 7segment
	 //i = 5;
	 
	 
	 while(k>=0){
	 	
		//if(k<length) set7Segment(string[length - k],TRUE);
		//delay();
		
		init();
		
		printf("%d",k);
	 	setLED(red_LED,TRUE); //turn on red LED
		delay();
		
		setLED(white_LED,TRUE); //turn on white LED
		delay();
		setLED(yellow_LED,TRUE);
		delay();
		setLED(green_LED,TRUE);
		delay();
		
	 	setLED(red_LED,FALSE); //turn off red LED
		delay();
		
		setLED(white_LED,FALSE); 
		delay(); 
		setLED(yellow_LED,FALSE); //turn off yellow LED
		delay();
		setLED(green_LED,FALSE);
		delay();
		//delayU(99999);
	    k = k -1;
	 }
	 //Lcd2PP_Init2();
 	 //while(1);
     
}

/*
Setup the ports DDRP,DDRH,and PTM to be able to detect a 
key press on the specified row. The row intialization is used in addition with a column check
to get the status of specific keys and whether they have 
been pressed or not. 
*/
void initRow(int rowid) {
	SPI1CR1 = 0x00;	  //clear Serial Peripheral Interface port
	
	DDRP = DDRP | 0x0f;	   //set outputs to key1-4, avoids turning on motor
	
	//clear DDRH
	DDRH = DDRH & 0x0f;	//sets columns as inputs
	
	//set u7 tp high
	PTM = PTM | 0x08;
	
	//load PTP with key
	if(rowid == 1) PTP = 0x01;   //if row 1 is passed in the parameter, set PTP accordingly 
	else if(rowid == 2) PTP = 0x02; //if row 2 is passed in the parameter, set PTP accordingly
	else if(rowid == 3) PTP = 0x04; //if row 3 is passed in the parameter, set PTP accordingly
	else if(rowid == 4) PTP = 0x08; //if row 4 is passed in the parameter, set PTP accordingly
	
	//set U7_EN low
	PTM = PTM & 0xf7; 
}

/*
Print the character on the board's LCD screen and increase 
the cursor to the next position.
*** This has been changed from question one. ***
*/
void printLCD(char c) {
	 LCD_display(c); //load the character
	 LCD_instruction(0x06); //write character and shift to next position
}

void printLCDSame(char c) 
{
	 LCD_display(c); //load the character
	 LCD_instruction(0x8C); //write character and shift to next position
}

/*
This checks the appropriate key on the board's keypad to see if it was pressed.
It does this only once, so it must be run in a loop [see function waiting() above].
Once it determines that a key has been pressed it runs the appropriate action
and returns the character which was pressed.
*/
char checkKey(int i) {
	 byte a = PTH & 0xf0;   //activates the columns in the keypad. (needed when polling the keys in a row) 
	 
	 //represent the columns of the array of keys (column 1 to 4)
	 byte col1 = 0x10;  //column 1
	 byte col2 = 0x20; //column 2
	 byte col3 = 0x40;	//column 3
	 byte col4 = 0x80;  //column 4
	 
	 
	 //row one of keys on keypad
	 if(i == 1){
	 	  //key '1' pressed
	 	  if (a==col1) {
					   return '1';
			}
		  
		  //key '2'pressed
	 	  else if (a==col2)return '2';
		  
		  //key '3' pressed 
	 	  else if (a==col3)return '3';
		  
		  //key 'A' pressed
	 	  else if (a==col4){
		  	  return 'A';
			  }
		  else return 0x00;
	 }
	 
	 //row two of keys on keypad
	 else if(i == 2){
	 	  //key '4' pressed
	 	  if (a==col1)return '4';
		  
		  //key '5' pressed
	 	  else if (a==col2)return '5';
		 
		 //key '6' pressed
	 	  else if (a==col3)return '6';
		  
		  //key 'B' pressed
	 	  else if (a==col4){
		  	   		   
			   return 'B';
		  }
		  else return 0x00;
	 }
	 
	  //row three of keys on keypad
	 else if(i == 3){
	 	  
		  ///key '7' pressed
	 	  if (a==col1)return '7';
		  
		  //key '8' pressed
	 	  else if (a==col2)return '8';
		  
		  //key '9' pressed
	 	  else if (a==col3)return '9';
		  
		  //key 'C' pressed
	  	  else if (a==col4){
		
			   return 'C';
		  }
		  else return 0x00;
	 }
	 
	 //row four of keys on keypad
	 else if(i == 4){
	  	  //letter E
	  	  if (a==col1) return 'E';
		  //letter 0
	 	  else if (a==col2) return '0';
		  //letter F
	 	  else if (a==col3){   
			   return 'F';
		  }
		  //letter D
	  	  else if (a==col4){
		  	 
			   return 'D';
		  }
		  else return 0x00;
	 }
	 return 0x00;
}



/*
Runs the simulation of 3 seconds a couple times with different values. 
*/
void main() {
	 
	 key = 0xFF;
	 //for keypad
	 DDRP |= 0x0F; // bitset PP0-3 as outputs (rows)  
	 DDRH &= 0x0F; // bitclear PH4..7 as inputs (columns)
	 PTP = 0x0F; // Set scan row(s)
	 //for interrupts
	 PIFH = 0xFF; // Clear previous interrupt flags
	 PPSH = 0xF0; // Rising Edge
	 PERH = 0x00; // Disable pulldowns
	 PIEH |= 0xF0; // Local enable on columns inputs

	 
	 //dialMode();
	 idleMode();
	 
}



// ***** START OF INTERRUPT VECTOR TABLE HERE ***** //

/* To use, either add this file to the project file list (preferred), or 
 * #include it in a single source file if you are just using 
 * File->CompileToOutput (this works but using the Project feature is better
 */

/* Vector Table for Dx256 S12 CPU
 * As is, all interrupts except reset jumps to 0xffff, which is most
 * likely not going to useful. To replace an entry, declare your function,
 * and then change the corresponding entry in the table. For example,
 * if you have a SCI handler (which must be defined with 
 * #pragma interrupt_handler ...) then in this file:
 * add
 *	extern void SCIHandler();
 * before th table.
 * In the SCI entry, change:
 *	DUMMY_ENTRY,
 * to
 *  SCIHandler, 
 */
#pragma nonpaged_function _start
extern void _start(void);	/* entry point in crt??.s */

//tells the compiler that _Timer_ISR is an interrupt and cannot be called directly
//#pragma interrupt_handler _Timer_ISR
//extern void _Timer_ISR(void); 

//tells the compiler that _Timer_ISR is an interrupt and cannot be called directly
#pragma interrupt_handler KISR
extern void KISR(void); 
#pragma interrupt_handler OC_TIMER
extern void OC_TIMER(void); 

#define NOICE_DUMMY_ENTRY (void (*)(void))0xF8CF

#define NOICE_XIRQ	(void (*)(void))0xF8C7
#define NOICE_SWI	(void (*)(void))0xF8C3
#define NOICE_TRAP	(void (*)(void))0xF8CB
#define NOICE_COP	(void (*)(void))0xF805
#define NOICE_CLM	(void (*)(void))0xF809

//#pragma abs_address:0xFF80 		//Used for Standalone Apps.
#pragma abs_address:0x3F8C 		//Used with the monitor

/* change the above address if your vector starts elsewhere
 */
void (*interrupt_vectors[])(void) = 
	{
	/* to cast a constant, say 0xb600, use
	   (void (*)())0xb600
	 */
	//NOICE_DUMMY_ENTRY, /*Reserved $FF80*/  //Not used under Monitor
	//NOICE_DUMMY_ENTRY, /*Reserved $FF82*/  //Not used under Monitor
	//NOICE_DUMMY_ENTRY, /*Reserved $FF84*/  //Not used under Monitor
	//NOICE_DUMMY_ENTRY, /*Reserved $FF86*/  //Not used under Monitor
	//NOICE_DUMMY_ENTRY, /*Reserved $FF88*/  //Not used under Monitor
	//NOICE_DUMMY_ENTRY, /*Reserved $FF8A*/  //Not used under Monitor
	NOICE_DUMMY_ENTRY, /*PWM Emergency Shutdown*/
	NOICE_DUMMY_ENTRY, /*Port P Interrupt*/
	NOICE_DUMMY_ENTRY, /*MSCAN 4 Transmit*/
	NOICE_DUMMY_ENTRY, /*MSCAN 4 Receive*/
	NOICE_DUMMY_ENTRY, /*MSCAN 4 Error*/
	NOICE_DUMMY_ENTRY, /*MSCAN 4 Wake-up*/
	NOICE_DUMMY_ENTRY, /*MSCAN 3 Transmit*/
	NOICE_DUMMY_ENTRY, /*MSCAN 3 Receive*/
	NOICE_DUMMY_ENTRY, /*MSCAN 3 Error*/
	NOICE_DUMMY_ENTRY, /*MSCAN 3 Wake-up*/
	NOICE_DUMMY_ENTRY, /*MSCAN 2 Transmit*/
	NOICE_DUMMY_ENTRY, /*MSCAN 2 Receive*/
	NOICE_DUMMY_ENTRY, /*MSCAN 2 Error*/
	NOICE_DUMMY_ENTRY, /*MSCAN 2 Wake-up*/
	NOICE_DUMMY_ENTRY, /*MSCAN 1 Transmit*/
	NOICE_DUMMY_ENTRY, /*MSCAN 1 Receive*/
	NOICE_DUMMY_ENTRY, /*MSCAN 1 Error*/
	NOICE_DUMMY_ENTRY, /*MSCAN 1 Wake-up*/
	NOICE_DUMMY_ENTRY, /*MSCAN 0 Transmit*/
	NOICE_DUMMY_ENTRY, /*MSCAN 0 Receive*/
	NOICE_DUMMY_ENTRY, /*MSCAN 0 Error*/
	NOICE_DUMMY_ENTRY, /*MSCAN 0 Wake-up*/
	NOICE_DUMMY_ENTRY, /*Flash*/
	NOICE_DUMMY_ENTRY, /*EEPROM*/
	NOICE_DUMMY_ENTRY, /*SPI2*/
	NOICE_DUMMY_ENTRY, /*SPI1*/
	NOICE_DUMMY_ENTRY, /*IIC Bus*/
	NOICE_DUMMY_ENTRY, /*DLC*/
	NOICE_DUMMY_ENTRY, /*SCME*/
	NOICE_DUMMY_ENTRY, /*CRG Lock*/
	NOICE_DUMMY_ENTRY, /*Pulse Accumulator B Overflow*/
	NOICE_DUMMY_ENTRY, /*Modulus Down Counter Underflow*/
	KISR, /*Port H Interrupt*/
	NOICE_DUMMY_ENTRY, /*Port J Interrupt*/
	NOICE_DUMMY_ENTRY, /*ATD1*/
	NOICE_DUMMY_ENTRY, /*ATD0*/
	NOICE_DUMMY_ENTRY, /*SCI1*/
	NOICE_DUMMY_ENTRY, /*SCI0*/
	NOICE_DUMMY_ENTRY, /*SPI0*/
	NOICE_DUMMY_ENTRY, /*Pulse Accumulator A Input Edge*/
	NOICE_DUMMY_ENTRY, /*Pulse Accumulator A Overflow*/
	NOICE_DUMMY_ENTRY, /*Timer Overflow*/
	NOICE_DUMMY_ENTRY, /*Timer Channel 7*/
	NOICE_DUMMY_ENTRY, /*Timer Channel 6*/
	NOICE_DUMMY_ENTRY, /*Timer Channel 5*/
	NOICE_DUMMY_ENTRY, /*Timer Channel 4*/
	NOICE_DUMMY_ENTRY, /*Timer Channel 3*/
	NOICE_DUMMY_ENTRY, /*Timer Channel 2*/
	NOICE_DUMMY_ENTRY, /*Timer Channel 1*/
	OC_TIMER, /*Timer Channel 0*/
	NOICE_DUMMY_ENTRY, /*Real Time Interrupt <---- THIS IS THE ONE WE ARE FOCUSED ON!*/ 
	NOICE_DUMMY_ENTRY, /*IRQ*/
	NOICE_XIRQ, /*XIRQ*/
	NOICE_SWI, /*SWI*/
	NOICE_TRAP, /*Unimplement Intruction Trap*/
	NOICE_COP, /*COP failure reset*/
	NOICE_CLM, /*Clock monitor fail reset*/
	_start, /*Reset*/
	};
#pragma end_abs_address
// ***** END OF INTERRUPT VECTOR TABLE HERE ***** //






/**
MODES TEMPLATE
 char next;
	 char  k;
	 Lcd2PP_Init();
	 
	 printLCDString("INCOMING CALL");

	 asm( "cli" );
	 for (;;){
	 	 while (key == 0xFF);
		 next = key;
		 Lcd2PP_Init2();
		
		 if(digitsEntered == 5){
		   //printf("maximum digits entered!");
		   dialingNumber();
		  
		   LCD_instruction(0x01); //clear display	 
		   printLCDString(" CONNECTED!");
		    setLED(green_LED,TRUE);
			while(1);
		  }
		    
		 else if((next <= '9') &&(next>='0')) {
		 		  printLCD(next);
				  ++digitsEntered;
		}
		
		
		 key = 0xFF;
	 }


**/
