	.module assign56_mobile.c
	.area data
_seconds::
	.blkb 2
	.area idata
	.word 0
	.area data
	.dbfile M:\2003\Assignment_5\assign56\assign56_mobile.c
	.dbsym e seconds _seconds I
_minutes::
	.blkb 2
	.area idata
	.word 0
	.area data
	.dbfile M:\2003\Assignment_5\assign56\assign56_mobile.c
	.dbsym e minutes _minutes I
_timeChanged::
	.blkb 2
	.area idata
	.word 0
	.area data
	.dbfile M:\2003\Assignment_5\assign56\assign56_mobile.c
	.dbsym e timeChanged _timeChanged I
_temperature_limit::
	.blkb 2
	.area idata
	.word 20
	.area data
	.dbfile M:\2003\Assignment_5\assign56\assign56_mobile.c
	.dbsym e temperature_limit _temperature_limit I
_blindsClosed::
	.blkb 1
	.area idata
	.byte 0
	.area data
	.dbfile M:\2003\Assignment_5\assign56\assign56_mobile.c
	.dbsym e blindsClosed _blindsClosed c
_numInterrupts::
	.blkb 2
	.area idata
	.word 1
	.area data
	.dbfile M:\2003\Assignment_5\assign56\assign56_mobile.c
	.dbsym e numInterrupts _numInterrupts I
_pressed::
	.blkb 2
	.area idata
	.word 0
	.area data
	.dbfile M:\2003\Assignment_5\assign56\assign56_mobile.c
	.dbsym e pressed _pressed I
_divisor::
	.blkb 2
	.area idata
	.word 0
	.area data
	.dbfile M:\2003\Assignment_5\assign56\assign56_mobile.c
	.dbsym e divisor _divisor I
_F_found::
	.blkb 2
	.area idata
	.word 0
	.area data
	.dbfile M:\2003\Assignment_5\assign56\assign56_mobile.c
	.dbsym e F_found _F_found I
_temperature::
	.blkb 2
	.area idata
	.word 20
	.area data
	.dbfile M:\2003\Assignment_5\assign56\assign56_mobile.c
	.dbsym e temperature _temperature I
_alarmArmed::
	.blkb 2
	.area idata
	.word 0
	.area data
	.dbfile M:\2003\Assignment_5\assign56\assign56_mobile.c
	.dbsym e alarmArmed _alarmArmed I
_windowSensor::
	.blkb 2
	.area idata
	.word 0
	.area data
	.dbfile M:\2003\Assignment_5\assign56\assign56_mobile.c
	.dbsym e windowSensor _windowSensor I
_InfraredSensor::
	.blkb 2
	.area idata
	.word 0
	.area data
	.dbfile M:\2003\Assignment_5\assign56\assign56_mobile.c
	.dbsym e InfraredSensor _InfraredSensor I
_heatingON::
	.blkb 1
	.area idata
	.byte 0
	.area data
	.dbfile M:\2003\Assignment_5\assign56\assign56_mobile.c
	.dbsym e heatingON _heatingON c
_temp::
	.blkb 1
	.area idata
	.byte 0
	.area data
	.dbfile M:\2003\Assignment_5\assign56\assign56_mobile.c
	.dbsym e temp _temp c
	.area text
	.dbfile M:\2003\Assignment_5\assign56\assign56_mobile.c
	.dbfunc e KISR _KISR fV
;           temp -> -1,x
_KISR::
	pshx
	tfr s,x
	leas -2,sp
	.dbline -1
	.dbline 111
; /**
; 
; 
; **/
; /* Assignment 5 in SYSC 2003  */
; /* AUTHORS: RYAN SEYS (100817604) AND OSAZUWA OMIGIE (100764733)*/
; 
; 
; #include <stdio.h>
; #include <hcs12dp256.h>
; 
; //macro: false defined as 0
; #define FALSE 0
; 
; //macro: true is anything that isn't 0 (not false)
; #define TRUE !0
; 
; //The maximum # of counts used to create a time delay.
; #define DELAY_MAX 11000    	   
; 
;  //delay macro here is just a for loop.
; #define delay(); for(i = 0; i < DELAY_MAX; i++);  	
; 
; #define red_LED 0
; #define white_LED 1
; #define yellow_LED 2
; #define green_LED 3	
; 
; //defining a boolean type and byte type
; typedef int boolean;
; 
; //VARIABLES
; int seconds = 0;
; int minutes = 0;
; boolean timeChanged = FALSE;
; //END VARIABLES
; 
; typedef unsigned char byte;  //Alias unsigned char data type as byte
; int temperature_limit = 20; // the temp "too high" value is initialized here. (higher or equal -> blinds closed, lower -> blinds open)
; byte blindsClosed = FALSE; //initially the blinds closed flag is false
; int numInterrupts = 1; // keeps track of the number of seconds that have passed.
; 
; void printLCDString(char *);
; 
; void onCallMode(void);
; 
; 
; 
; //RECOUP 3 VARIABLES //
; char key;
; boolean pressed = FALSE;
; 
; //END OF RECOUP 3 VARIABLES //
; 
; 
; // *** This has changed from question one. ***
; //QUESTION 2 FUNCTION PROTOTYPES //
; void printLCD(char); 
; void LCD_display(char);
; void LCD_instruction(byte);
; void Lcd2PP_Init(void);
; void Lcd2PP_Init2(void);
; extern void setLED( unsigned byte, boolean );
; extern void set7Segment( char number, boolean on ) ;
; // END OF QUESTION 2 FUNCTION PROTOTYPES //
; 
; // this divisor is used in conjuction with the interrupt to tell it when
; // the numInterrupts should increase.
; int divisor = 0; 
; 
; // QUESTION 1 VARIABLES //
; int F_found = 0; //F key has not been pressed to begin.
; int temperature = 20; //initial temperature is 20 degrees Celcius
; boolean alarmArmed = FALSE; //alarm is disarmed to begin
; boolean windowSensor = FALSE; //window is not open to begin
; boolean InfraredSensor = FALSE; //presense not detected to begin
; // END OF QUESTION 1 VARIABLES //
; 
; // *** This has changed from question one. ***
; // QUESTION 2 VARIABLES //
; byte heatingON = FALSE; //heating is initialized as OFF.
; // END OF QUESTION 2 VARIABLES //
; 
; // QUESTION 1 FUNCTION PROTOTYPES //
; void waiting(void);
; void initRow(int);
; char checkKey(int);
; 
; //void delay (int);
; // END OF QUESTION 1 FUNCTION PROTOTYPES //
; 
; // PROTOTYPES HERE
; void runSimulation(void);
; void STEPPERClkwise(int);
; void STEPPERAntiClkwise(int);
; char temp = 0x00;
; 
; // RECOUP3 PROTOTYPES
; char getASCII(void); // Gets key and converts to ASCII
; void printLCDSame(char);
; // END OF RECOUP3 PROTOTYPES
; 
; /* The Timer ISR is called every time the real time interrupt occurs! 
;    In this case it counts how many times it has occurred. 
;    numInterrupts is increased by 1 approximately once per second.
;    The interrupt handler is used in causing a 3 second delay.
; */
; //void _Timer_ISR(void) {
; 
; #pragma interrupt_handler KISR()
; void KISR(void) {
	.dbline 112
; 	 char temp = 0x00;
	clr -1,x
	.dbline 113
; 	 PIEH = 0x00;   // Local disable
	clr 0x266
	.dbline 115
; 	 //pressed = TRUE;
; 	 temp = getASCII();
	jsr _getASCII
	stab -1,x
	.dbline 116
; 	 if(temp != 0x00) key = temp;
	tst -1,x
	beq L4
	.dbline 116
	movb -1,x,_key
L4:
	.dbline 118
; 	 
; 	 PTM = PTM | 0x08;
	bset 0x250,#8
	.dbline 119
; 	 PTP = 0x0F; //set scan rows
	ldab #15
	stab 0x258
	.dbline 121
; 	 //set U7_EN low
; 	 PTM = PTM & 0xf7;
	bclr 0x250,#0x8
	.dbline 122
; 	 DDRP |= 0x0F; // bitset PP0-3 as outputs (rows)
	bset 0x25a,#15
	.dbline 125
; 	 //DDRH &= 0x0F; // bitclear PH4..7 as inputs (columns)
; 	 
; 	 PIEH |= 0xF0; // Local enable on columns input
	bset 0x266,#240
	.dbline 126
; 	 PIFH = PIFH; // Acknowledge (all) interrupts
	; vol
	ldab 0x267
	stab 0x267
	.dbline -2
	.dbline 128
; 	 //asm("rti");
; }
L3:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rti
	.dbsym l temp -1 c
	.dbend
	.dbfunc e getASCII _getASCII fc
;           temp -> -3,x
;              i -> -2,x
_getASCII::
	pshx
	tfr s,x
	leas -4,sp
	.dbline -1
	.dbline 130
; 
; char getASCII(void) {
	.dbline 131
; 	 int i = 1;
	ldd #1
	std -2,x
	.dbline 132
; 	 char temp = 0x00;  //initialize temp to 0
	clr -3,x
	bra L8
L7:
	.dbline 134
; 	 while(temp == 0x00) 
; 	 {
	.dbline 135
; 	 	   initRow(i);  //initialize row i
	ldd -2,x
	jsr _initRow
	.dbline 138
; 		   
; 		   //poll the keypad to see which which key was pressed
; 		   temp = checkKey(i);   //store the key pressed in temp
	ldd -2,x
	jsr _checkKey
	stab -3,x
	.dbline 139
; 		   i++;     
	ldd -2,x
	addd #1
	std -2,x
	.dbline 140
; 		   if(i > 4) break;  //break from the loop after checking the 4th row
	ldd -2,x
	cpd #4
	ble L10
	.dbline 140
	bra L9
L10:
	.dbline 141
L8:
	.dbline 133
	tst -3,x
	beq L7
L9:
	.dbline 142
; 	 }
; 	 return temp;
	ldab -3,x
	clra
	.dbline -2
L6:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rts
	.dbsym l temp -3 c
	.dbsym l i -2 I
	.dbend
	.dbfunc e printLCDString _printLCDString fV
;         length -> -4,x
;              i -> -2,x
;         string -> 2,x
_printLCDString::
	pshd
	pshx
	tfr s,x
	leas -4,sp
	.dbline -1
	.dbline 145
; }
; 
; void printLCDString(char string[]) {
	.dbline 147
; 	 int i;
; 	 int length = strlen(string);
	ldd 2,x
	jsr _strlen
	std -4,x
	.dbline 148
	ldd #0
	std -2,x
	bra L16
L13:
	.dbline 148
	.dbline 149
	ldd -2,x
	addd 2,x
	xgdy
	ldab 0,y
	clra
	jsr _printLCD
	.dbline 150
L14:
	.dbline 148
	ldd -2,x
	addd #1
	std -2,x
L16:
	.dbline 148
; 	 for(i = 0; i < length; i++) {
	ldd -2,x
	cpd -4,x
	blt L13
	.dbline -2
	.dbline 151
; 	  	   printLCD(string[i]);
; 	 }
; }
L12:
	tfr x,s
	pulx
	leas 2,sp
	.dbline 0 ; func end
	rts
	.dbsym l length -4 I
	.dbsym l i -2 I
	.dbsym l string 2 pc
	.dbend
	.dbfunc e delayU _delayU fV
;       delayVal -> -2,x
;            val -> 2,x
_delayU::
	pshd
	pshx
	tfr s,x
	leas -2,sp
	.dbline -1
	.dbline 154
; 
; 
; void delayU(int val) {
	.dbline 156
	ldd #0
	std -2,x
	bra L21
L18:
	.dbline 156
L19:
	.dbline 156
	ldd -2,x
	addd #1
	std -2,x
L21:
	.dbline 156
;      int delayVal;
; 	 for(delayVal = 0; delayVal < val; delayVal++);
	ldd -2,x
	cpd 2,x
	blt L18
	.dbline -2
	.dbline 157
; }
L17:
	tfr x,s
	pulx
	leas 2,sp
	.dbline 0 ; func end
	rts
	.dbsym l delayVal -2 I
	.dbsym l val 2 I
	.dbend
	.dbfunc e idleMode _idleMode fV
;              k -> -2,x
;           next -> -1,x
_idleMode::
	pshx
	tfr s,x
	leas -2,sp
	.dbline -1
	.dbline 160
; 
; 
; void idleMode(){
	.dbline 164
; 
; 	 char next;
; 	 char  k;
; 	 Lcd2PP_Init();
	jsr _Lcd2PP_Init
	.dbline 165
; 	 writeStartScreen();
	jsr _writeStartScreen
	.dbline 167
; 
; 	 asm( "cli" );
		cli

	.dbline 168
; 	 for (;;){
	.dbline 168
L27:
	.dbline 172
L28:
	.dbline 172
; 	 	 //Lcd2PP_Init(); 
;          
; 		 
; 	 	 while (key == 0xFF); //
	ldab _key
	cmpb #255
	beq L27
	.dbline 173
; 		 next = key;
	movb _key,-1,x
	.dbline 176
; 		 //Lcd2PP_Init2();
; 		
; 		if(next == 'C'){
	ldab -1,x
	cmpb #67
	bne L30
	.dbline 176
	.dbline 177
; 		  	incomingCallMode();
	jsr _incomingCallMode
	.dbline 178
; 			writeStartScreen();
	jsr _writeStartScreen
	.dbline 180
; 		    
; 		}
	bra L31
L30:
	.dbline 181
; 		else if((next == 'D') || ((next >= '0') && (next <='9') ))
	ldab -1,x
	cmpb #68
	beq L34
	ldab -1,x
	cmpb #48
	blo L32
	ldab -1,x
	cmpb #57
	bhi L32
L34:
	.dbline 182
; 		{
	.dbline 183
; 		  	dialMode(); 
	jsr _dialMode
	.dbline 184
; 			printf("done dialing"); 
	ldd #L35
	jsr _printf
	.dbline 185
; 			writeStartScreen();
	jsr _writeStartScreen
	.dbline 186
; 		}
L32:
L31:
	.dbline 187
	ldab #255
	stab _key
	.dbline 188
	.dbline 168
	.dbline 168
	bra L28
X0:
	.dbline -2
L22:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rts
	.dbsym l k -2 c
	.dbsym l next -1 c
	.dbend
	.dbfunc e writeStartScreen _writeStartScreen fV
_writeStartScreen::
	.dbline -1
	.dbline 192
; 		 key = 0xFF;
; 	 }
; 
; }
; 
; void writeStartScreen() {
	.dbline 193
;     LCD_instruction(0x01); //clear display	 
	ldd #1
	jsr _LCD_instruction
	.dbline 194
;     printLCDString("<<OSYAN MOBILE>>"); 
	ldd #L37
	jsr _printLCDString
	.dbline 195
;     LCD_instruction(0xc0);
	ldd #192
	jsr _LCD_instruction
	.dbline 196
;     printLCDString("Call D:out C:in");
	ldd #L38
	jsr _printLCDString
	.dbline -2
	.dbline 197
; }
L36:
	.dbline 0 ; func end
	rts
	.dbend
	.dbfunc e delay1second _delay1second fV
;              i -> -2,x
_delay1second::
	pshx
	tfr s,x
	leas -2,sp
	.dbline -1
	.dbline 198
; void delay1second() {
	.dbline 200
; 	 int i;
; 	 TSCR1 = 0x90; // movb #$90,TSCR ; enable TCNT and fast timer
	ldab #144
	stab 0x46
	.dbline 201
; 	 TIE = 0x03; // movb #$05,TMSK2 ; set prescale factor to 8 (3 = 8, 5 = 32)
	ldab #3
	stab 0x4c
	.dbline 202
; 	 TIOS |= 0x01; //enable OC0
	bset 0x40,#1
	.dbline 203
; 	 TC0 = TCNT;
	; vol
	ldd 0x44
	std 0x50
	.dbline 205
	ldd #0
	std -2,x
L40:
	.dbline 205
; 	 //by experiment it shows that the frequency is actually low
; 	 for(i = 0; i < 125; i++) { 
	.dbline 206
; 	 	   TC0 += 50000;
	; vol
	ldd 0x50
	addd #0xc350
	std 0x50
L44:
	.dbline 207
L45:
	.dbline 207
	brclr 0x4e,#1,L44
	.dbline 208
L41:
	.dbline 205
	ldd -2,x
	addd #1
	std -2,x
	.dbline 205
	ldd -2,x
	cpd #125
	blt L40
	.dbline -2
	.dbline 209
; 		   while((TFLG1 & 0x01) == 0);
; 	 }
; }
L39:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rts
	.dbsym l i -2 I
	.dbend
	.dbfunc e dialMode _dialMode fV
;              k -> -9,x
;     phoneNumbr -> -8,x
;  digitsEntered -> -3,x
;           next -> -1,x
_dialMode::
	pshx
	tfr s,x
	leas -12,sp
	.dbline -1
	.dbline 211
; 
; void dialMode(){
	.dbline 212
; 	 int digitsEntered = 0;
	ldd #0
	std -3,x
	.dbline 216
;  	 char phoneNumbr[5];
; 	 char next;
; 	 char  k;
; 	 key = 0xFF;
	ldab #255
	stab _key
	.dbline 220
;  
; 	 
; 	 //Lcd2PP_Init();
; 	 LCD_instruction(0x01); //clear display	 
	ldd #1
	jsr _LCD_instruction
	.dbline 221
;  	 LCD_instruction(0x0C); //turn of cursor and blink
	ldd #12
	jsr _LCD_instruction
	.dbline 222
; 	 printLCDString("Dial:");
	ldd #L48
	jsr _printLCDString
	.dbline 223
; 	 LCD_instruction(0xc0);
	ldd #192
	jsr _LCD_instruction
	.dbline 224
; 	 printLCDString("Enter phone #");
	ldd #L49
	jsr _printLCDString
	.dbline 226
; 	 
; 	 LCD_instruction(0x86);
	ldd #134
	jsr _LCD_instruction
	.dbline 228
; 	 
; 	 asm( "cli" );
		cli

	.dbline 229
L50:
	.dbline 229
; 	 for (;;) {
	.dbline 230
; 	 	 if(digitsEntered == 5){
	ldd -3,x
	cpd #5
	bne L57
	.dbline 230
	.dbline 232
; 		   //printf("maximum digits entered!");
; 		   dialingNumber(phoneNumbr);
	leay -8,x
	xgdy
	jsr _dialingNumber
	.dbline 233
; 		   onCallMode();
	jsr _onCallMode
	.dbline 234
; 		   return;
	bra L47
L56:
	.dbline 238
L57:
	.dbline 238
; 		   
; 		   
; 		  }
; 	 	 while (key == 0xFF);
	ldab _key
	cmpb #255
	beq L56
	.dbline 239
; 		 next = key;
	movb _key,-1,x
	.dbline 242
; 		 //Lcd2PP_Init2();
; 		
; 		 if((next <= '9') &&(next>='0')) {
	ldab -1,x
	cmpb #57
	bhi L59
	ldab -1,x
	cmpb #48
	blo L59
	.dbline 242
	.dbline 243
; 		 		  printLCD(next);
	ldab -1,x
	clra
	jsr _printLCD
	.dbline 244
; 				  phoneNumbr[digitsEntered] = next;
	leay -8,x
	sty -12,x
	ldd -3,x
	addd -12,x
	xgdy
	ldab -1,x
	stab 0,y
	.dbline 245
; 				  ++digitsEntered;
	ldd -3,x
	addd #1
	std -3,x
	.dbline 246
; 		}
	bra L60
L59:
	.dbline 248
; 		 
; 		  else if(next == 'C'){
	ldab -1,x
	cmpb #67
	bne L61
	.dbline 248
	.dbline 249
; 		  	incomingCallMode();   
	jsr _incomingCallMode
	.dbline 251
; 		    //Lcd2PP_Init2(); 
; 			LCD_instruction(0x01); //clear display	
	ldd #1
	jsr _LCD_instruction
	.dbline 252
; 			printLCDString("Dial:");
	ldd #L48
	jsr _printLCDString
	.dbline 253
; 		  }
	bra L62
L61:
	.dbline 255
; 		  
; 		  else if(next == 'D'){
	ldab -1,x
	cmpb #68
	bne L63
	.dbline 255
	.dbline 257
; 		  	   //idleMode();
; 			   return;
	bra L47
L63:
L62:
L60:
	.dbline 260
	ldab #255
	stab _key
	.dbline 261
	.dbline 229
	.dbline 229
	bra L50
X1:
	.dbline -2
L47:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rts
	.dbsym l k -9 c
	.dbsym l phoneNumbr -8 A[5:5]c
	.dbsym l digitsEntered -3 I
	.dbsym l next -1 c
	.dbend
	.dbfunc e incomingCallMode _incomingCallMode fV
;              k -> -2,x
;           next -> -1,x
_incomingCallMode::
	pshx
	tfr s,x
	leas -4,sp
	.dbline -1
	.dbline 266
; 			   
; 		  }
; 		 key = 0xFF;
; 	 }
; 
; }
; 
; void incomingCallMode()
; {
	.dbline 272
;  //int digitsEntered = 0;
; 	 char next;
; 	 char  k;
; 	 //key = 0xFF;
; 	 //Lcd2PP_Init2();
; 	 LCD_instruction(0x01); //clear display	 
	ldd #1
	jsr _LCD_instruction
	.dbline 274
; 	 
; 	 key = 0xFF; //don't recogniz keys pressed before the alerts displays
	ldab #255
	stab _key
	.dbline 277
; 	 //printLCDString("INCOMING CALL!");
; 
; 	 asm( "cli" );
		cli

	.dbline 280
; 	 //for (;;){
; 	 	 
; 	 init();
	jsr _init
	.dbline 281
; 	 setLED(green_LED,FALSE);
	ldd #0
	std 0,sp
	ldd #3
	jsr _setLED
	.dbline 282
; 	 setLED(red_LED,TRUE);
	ldd #1
	std 0,sp
	ldd #0
	jsr _setLED
	bra L67
L66:
	.dbline 283
	.dbline 284
	ldd #1
	jsr _LCD_instruction
	.dbline 285
	ldd #L69
	jsr _printLCDString
	.dbline 286
	ldd #192
	jsr _LCD_instruction
	.dbline 287
	ldd #L70
	jsr _printLCDString
	.dbline 288
	jsr _ringTone
	.dbline 292
	ldd #300
	jsr _delayU
	.dbline 293
L67:
	.dbline 283
; 		 while ( (key != 'D') && (key != 'A') ){
	ldab _key
	cmpb #68
	beq L71
	ldab _key
	cmpb #65
	bne L66
L71:
	.dbline 294
; 		   LCD_instruction(0x01); //clear display
; 		   printLCDString(" INCOMING CALL!");
; 		   LCD_instruction(0xc0);
; 		   printLCDString("D:Deny, A:Answer");
; 		   ringTone();
; 		   //setLED(red_LED,TRUE);
; 		   //delayU(10);
; 		   //setLED(red_LED,FALSE);
;   		   delayU(300);
; 		 }  
; 		 next = key; //value of key may change in the process so store its current value
	movb _key,-1,x
	.dbline 297
; 		 //Lcd2PP_Init2();
; 		
; 		 if (next == 'D' ){
	ldab -1,x
	cmpb #68
	bne L72
	.dbline 297
	.dbline 298
; 		 	LCD_instruction(0x01); //clear display
	ldd #1
	jsr _LCD_instruction
	.dbline 299
; 			setLED(red_LED,FALSE);
	ldd #0
	std 0,sp
	ldd #0
	jsr _setLED
	.dbline 300
; 		 	printLCDString("Call Rejected");
	ldd #L74
	jsr _printLCDString
	.dbline 303
; 			//while(1);
; 			//delayU(200);
; 			delay1second();
	jsr _delay1second
	.dbline 304
; 			key = 0xFF;
	ldab #255
	stab _key
	.dbline 305
; 			return;
	bra L65
L72:
	.dbline 307
; 		 } 
; 		 else if (next == 'A' ){
	ldab -1,x
	cmpb #65
	bne L75
	.dbline 307
	.dbline 312
; 		 	//printLCDString("ON CALL");
; 			//setLED(red_LED,FALSE);
; 			//delayU(200);
; 			
; 			onCallMode();
	jsr _onCallMode
	.dbline 313
; 		 }
L75:
	.dbline 314
; 		key = 0xFF;
	ldab #255
	stab _key
	.dbline -2
	.dbline 315
; }
L65:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rts
	.dbsym l k -2 c
	.dbsym l next -1 c
	.dbend
	.dbfunc e printTime _printTime fV
_printTime::
	.dbline -1
	.dbline 317
; 
; void printTime() {
	.dbline 318
; 	 printLCD(minutes / 10 + 0x30); //get the MSB and convert to ascii. then print it out.
	ldd _minutes
	ldy #10
	exg x,y
	idivs
	exg x,y
	xgdy
	addd #48
	clra
	jsr _printLCD
	.dbline 319
; 	 printLCD(minutes % 10 + 0x30); //get LSB and convert to ascii. then print it out.
	ldd _minutes
	ldy #10
	exg x,y
	idivs
	exg x,y
	addd #48
	clra
	jsr _printLCD
	.dbline 320
; 	 printLCD(':');
	ldd #58
	jsr _printLCD
	.dbline 322
; 	 //Print out the value of the seconds onto the LCD
; 	 printLCD(seconds / 10 + 0x30); //get the MSB and convert to ascii. then print it out.
	ldd _seconds
	ldy #10
	exg x,y
	idivs
	exg x,y
	xgdy
	addd #48
	clra
	jsr _printLCD
	.dbline 323
; 	 printLCD(seconds % 10 + 0x30); //get LSB and convert to ascii. then print it out.
	ldd _seconds
	ldy #10
	exg x,y
	idivs
	exg x,y
	addd #48
	clra
	jsr _printLCD
	.dbline -2
	.dbline 324
; }
L77:
	.dbline 0 ; func end
	rts
	.dbend
	.dbfunc e onCallMode _onCallMode fV
;           next -> -1,x
_onCallMode::
	pshx
	tfr s,x
	leas -4,sp
	.dbline -1
	.dbline 327
; 
; 
; void onCallMode(){
	.dbline 330
; 	 
; 	 char next;
; 	 key = 0xFF;
	ldab #255
	stab _key
	.dbline 331
; 	 LCD_instruction(0x01); //clear display	
	ldd #1
	jsr _LCD_instruction
	.dbline 332
; 	 printLCDString(" ON CALL");
	ldd #L79
	jsr _printLCDString
	.dbline 333
; 	 LCD_instruction(0xc0);
	ldd #192
	jsr _LCD_instruction
	.dbline 334
; 	 printLCDString("E: End Call");
	ldd #L80
	jsr _printLCDString
	.dbline 335
; 	 LCD_instruction(0x88);
	ldd #136
	jsr _LCD_instruction
	.dbline 336
; 	 printLCDString(" T:");
	ldd #L81
	jsr _printLCDString
	.dbline 337
; 	 seconds = 0;
	ldd #0
	std _seconds
	.dbline 338
; 	 minutes = 0;
	ldd #0
	std _minutes
	.dbline 343
; 	 //PORTK |= (1<<6);
;  	 //PORTK |= (1<<7);
; 	 
;      //printLCDString("endCall[D]");	 
;      setLED(red_LED,FALSE);
	ldd #0
	std 0,sp
	ldd #0
	jsr _setLED
	.dbline 344
; 	 setLED(green_LED,TRUE);
	ldd #1
	std 0,sp
	ldd #3
	jsr _setLED
	.dbline 345
; 	 for(;;){
	.dbline 345
	bra L87
L86:
	.dbline 346
; 	  while(key == 0xFF) {
	.dbline 349
; 	  //while(timeChanged == FALSE);
; 		 //reinitialize ports for the keypad.
; 		 seconds++;
	ldd _seconds
	addd #1
	std _seconds
	.dbline 351
; 		 //printf("seconds increased!");
;     	 if(seconds == 60) seconds = 0; //roll over every minute
	ldd _seconds
	cpd #60
	bne L89
	.dbline 351
	ldd #0
	std _seconds
L89:
	.dbline 352
;     	 if(seconds == 0) minutes++;
	ldd _seconds
	bne L91
	.dbline 352
	ldd _minutes
	addd #1
	std _minutes
L91:
	.dbline 353
;     	 if(minutes == 60) minutes = 0; //roll over every hour
	ldd _minutes
	cpd #60
	bne L93
	.dbline 353
	ldd #0
	std _minutes
L93:
	.dbline 355
	ldd #139
	jsr _LCD_instruction
	.dbline 356
	jsr _printTime
	.dbline 357
	jsr _delay1second
	.dbline 359
L87:
	.dbline 346
	ldab _key
	cmpb #255
	beq L86
	.dbline 360
;     	 /* print to LCD here */
;     	 LCD_instruction(0x8B); //MSB of minutes
;     	 printTime();
; 		 delay1second();
;     	 //timeChanged = FALSE;
; 	  }
; 	  next = key;
	movb _key,-1,x
	.dbline 362
; 	  
; 	  if(next == 'E') { 
	ldab -1,x
	cmpb #69
	bne L95
	.dbline 362
	.dbline 363
; 	  		  key = 0xFF; 
	ldab #255
	stab _key
	.dbline 364
; 			  LCD_instruction(0x01); //clear display	
	ldd #1
	jsr _LCD_instruction
	.dbline 365
; 	 		  printLCDString(" CALL ENDED! ");
	ldd #L97
	jsr _printLCDString
	.dbline 366
; 			  LCD_instruction(0xc0);
	ldd #192
	jsr _LCD_instruction
	.dbline 367
; 			  printLCDString("Duration:");
	ldd #L98
	jsr _printLCDString
	.dbline 368
; 			  printTime();
	jsr _printTime
	.dbline 369
; 			  delay1second(); //delay
	jsr _delay1second
	.dbline 370
; 			  delay1second(); //delay
	jsr _delay1second
	.dbline 371
; 			  return; 
	bra L78
L95:
	.dbline 374
	ldab #255
	stab _key
	.dbline 375
	.dbline 345
	.dbline 345
	bra L87
X2:
	.dbline -2
L78:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rts
	.dbsym l next -1 c
	.dbend
	.dbfunc e OC_TIMER _OC_TIMER fV
_OC_TIMER::
	.dbline -1
	.dbline 379
; 			  
; 	  } //hangup 
; 	  else key = 0xFF; //go back to timing
; 	 }
; }
; 
; #pragma interrupt_handler OC_TIMER
; void OC_TIMER(void) {
	.dbline 380
; 	 TC0 = TCNT;
	; vol
	ldd 0x44
	std 0x50
	.dbline -2
	.dbline 381
; }
L99:
	.dbline 0 ; func end
	rti
	.dbend
	.dbfunc e ringTone _ringTone fV
;              x -> -4,x
;              i -> -2,x
_ringTone::
	pshx
	tfr s,x
	leas -4,sp
	.dbline -1
	.dbline 384
; 
; /* Sound the buzzer on the board for one second. */
; void ringTone() {
	.dbline 387
; 	int i; //for the delay macro
; 	int x; //for the for-loop
; 	DDRK |= (1 << 5); 
	bset 0x33,#32
	.dbline 390
; 	//DDRK |=  // outputs for the buzzer set
; 	//PORTK |= 0x20; //turning on the buzzer bit
; 	PORTK |= (1 << 5); //turning on the buzzer bit
	bset 0x32,#32
	.dbline 394
	ldd #0
	std -4,x
L101:
	.dbline 394
; 
; 	
; 	//delay here
; 	for(x = 0; x < 12; x++) {
	.dbline 395
	.dbline 395
	ldd #0
	std -2,x
L105:
	.dbline 395
L106:
	.dbline 395
	ldd -2,x
	addd #1
	std -2,x
	.dbline 395
	ldd -2,x
	cpd #11000
	blt L105
	.dbline 395
	.dbline 396
L102:
	.dbline 394
	ldd -4,x
	addd #1
	std -4,x
	.dbline 394
	ldd -4,x
	cpd #12
	blt L101
	.dbline 399
; 		  delay(); 
; 	}
; 	
; //	PORTK |= 0x00;  
; 	PORTK &= ~(1 << 5); //turn off the buzzer afterwards
	bclr 0x32,#0x20
	.dbline 401
; 	
; 	DDRK &= ~(1 << 5); // outputs
	bclr 0x33,#0x20
	.dbline -2
	.dbline 402
; }
L100:
	tfr x,s
	pulx
	.dbline 0 ; func end
	rts
	.dbsym l x -4 I
	.dbsym l i -2 I
	.dbend
	.dbfunc e dialingNumber _dialingNumber fV
;         length -> -8,x
;              w -> -6,x
;              k -> -4,x
;              i -> -2,x
;         string -> 2,x
_dialingNumber::
	pshd
	pshx
	tfr s,x
	leas -10,sp
	.dbline -1
	.dbline 405
; 
; 
; void dialingNumber(char string[]){
	.dbline 408
; 	 int i;
; 	 int w;
; 	 int k = 6;
	ldd #6
	std -4,x
	.dbline 409
;  	 int length = strlen(string);
	ldd 2,x
	jsr _strlen
	std -8,x
	.dbline 413
; 	 //Lcd2PP_Init();
; //	 while(1){
; 	 
;  	 LCD_instruction(0x01); //clear display	 
	ldd #1
	jsr _LCD_instruction
	.dbline 414
; 	 printLCDString("DIALING ");
	ldd #L110
	jsr _printLCDString
	.dbline 418
	ldd #0
	std -6,x
	bra L114
L111:
	.dbline 418
	.dbline 419
	ldd -6,x
	addd 2,x
	xgdy
	ldab 0,y
	clra
	jsr _printLCD
	.dbline 420
L112:
	.dbline 418
	ldd -6,x
	addd #1
	std -6,x
L114:
	.dbline 418
; 	 
; 
; 	 
; 	 for(w = 0; w < length; w++) {
	ldd -6,x
	cpd -8,x
	blt L111
	.dbline 422
; 	  	   printLCD(string[w]);
; 	 }
; 	 
; 	 printLCDString("...");
	ldd #L115
	jsr _printLCDString
	.dbline 424
; 	 
; 	 init(); //intializations for LED and 7segment
	jsr _init
	lbra L117
L116:
	.dbline 428
; 	 //i = 5;
; 	 
; 	 
; 	 while(k>=0){
	.dbline 433
; 	 	
; 		//if(k<length) set7Segment(string[length - k],TRUE);
; 		//delay();
; 		
; 		init();
	jsr _init
	.dbline 435
; 		
; 		printf("%d",k);
	movw -4,x,0,sp
	ldd #L119
	jsr _printf
	.dbline 436
; 	 	setLED(red_LED,TRUE);
	ldd #1
	std 0,sp
	ldd #0
	jsr _setLED
	.dbline 437
	.dbline 437
	ldd #0
	std -2,x
L120:
	.dbline 437
L121:
	.dbline 437
; 		delay();
	ldd -2,x
	addd #1
	std -2,x
	.dbline 437
	ldd -2,x
	cpd #11000
	blt L120
	.dbline 437
	.dbline 439
; 		
; 		setLED(white_LED,TRUE);
	ldd #1
	std 0,sp
	ldd #1
	jsr _setLED
	.dbline 440
	.dbline 440
	ldd #0
	std -2,x
L124:
	.dbline 440
L125:
	.dbline 440
; 		delay();
	ldd -2,x
	addd #1
	std -2,x
	.dbline 440
	ldd -2,x
	cpd #11000
	blt L124
	.dbline 440
	.dbline 441
; 		setLED(yellow_LED,TRUE);
	ldd #1
	std 0,sp
	ldd #2
	jsr _setLED
	.dbline 442
	.dbline 442
	ldd #0
	std -2,x
L128:
	.dbline 442
L129:
	.dbline 442
; 		delay();
	ldd -2,x
	addd #1
	std -2,x
	.dbline 442
	ldd -2,x
	cpd #11000
	blt L128
	.dbline 442
	.dbline 443
; 		setLED(green_LED,TRUE);
	ldd #1
	std 0,sp
	ldd #3
	jsr _setLED
	.dbline 444
	.dbline 444
	ldd #0
	std -2,x
L132:
	.dbline 444
L133:
	.dbline 444
; 		delay();
	ldd -2,x
	addd #1
	std -2,x
	.dbline 444
	ldd -2,x
	cpd #11000
	blt L132
	.dbline 444
	.dbline 446
; 		
; 	 	setLED(red_LED,FALSE);
	ldd #0
	std 0,sp
	ldd #0
	jsr _setLED
	.dbline 447
	.dbline 447
	ldd #0
	std -2,x
L136:
	.dbline 447
L137:
	.dbline 447
; 		delay();
	ldd -2,x
	addd #1
	std -2,x
	.dbline 447
	ldd -2,x
	cpd #11000
	blt L136
	.dbline 447
	.dbline 449
; 		
; 		setLED(white_LED,FALSE);
	ldd #0
	std 0,sp
	ldd #1
	jsr _setLED
	.dbline 450
	.dbline 450
	ldd #0
	std -2,x
L140:
	.dbline 450
L141:
	.dbline 450
; 		delay();
	ldd -2,x
	addd #1
	std -2,x
	.dbline 450
	ldd -2,x
	cpd #11000
	blt L140
	.dbline 450
	.dbline 451
; 		setLED(yellow_LED,FALSE);
	ldd #0
	std 0,sp
	ldd #2
	jsr _setLED
	.dbline 452
	.dbline 452
	ldd #0
	std -2,x
L144:
	.dbline 452
L145:
	.dbline 452
; 		delay();
	ldd -2,x
	addd #1
	std -2,x
	.dbline 452
	ldd -2,x
	cpd #11000
	blt L144
	.dbline 452
	.dbline 453
; 		setLED(green_LED,FALSE);
	ldd #0
	std 0,sp
	ldd #3
	jsr _setLED
	.dbline 454
	.dbline 454
	ldd #0
	std -2,x
L148:
	.dbline 454
L149:
	.dbline 454
	ldd -2,x
	addd #1
	std -2,x
	.dbline 454
	ldd -2,x
	cpd #11000
	blt L148
	.dbline 454
	.dbline 456
	ldd -4,x
	subd #1
	std -4,x
	.dbline 457
L117:
	.dbline 428
	ldd -4,x
	lbge L116
	.dbline -2
	.dbline 461
; 		delay();
; 		//delayU(99999);
; 	    k = k -1;
; 	 }
; 	 //Lcd2PP_Init2();
;  	 //while(1);
;      
; }
L109:
	tfr x,s
	pulx
	leas 2,sp
	.dbline 0 ; func end
	rts
	.dbsym l length -8 I
	.dbsym l w -6 I
	.dbsym l k -4 I
	.dbsym l i -2 I
	.dbsym l string 2 pc
	.dbend
	.dbfunc e initRow _initRow fV
;          rowid -> 2,x
_initRow::
	pshd
	pshx
	tfr s,x
	.dbline -1
	.dbline 469
; 
; /*
; Setup the ports DDRP,DDRH,and PTM to be able to detect a 
; key press on the specified row. The row intialization is used in addition with a column check
; to get the status of specific keys and whether they have 
; been pressed or not. 
; */
; void initRow(int rowid) {
	.dbline 470
; 	SPI1CR1 = 0x00;	  //clear Serial Peripheral Interface port
	clr 0xf0
	.dbline 472
; 	
; 	DDRP = DDRP | 0x0f;	   //set outputs to key1-4, avoids turning on motor
	bset 0x25a,#15
	.dbline 475
; 	
; 	//clear DDRH
; 	DDRH = DDRH & 0x0f;	//sets columns as inputs
	bclr 0x262,#0xf0
	.dbline 478
; 	
; 	//set u7 tp high
; 	PTM = PTM | 0x08;
	bset 0x250,#8
	.dbline 481
; 	
; 	//load PTP with key
; 	if(rowid == 1) PTP = 0x01;   //if row 1 is passed in the parameter, set PTP accordingly 
	ldd 2,x
	cpd #1
	bne L153
	.dbline 481
	ldab #1
	stab 0x258
	bra L154
L153:
	.dbline 482
; 	else if(rowid == 2) PTP = 0x02; //if row 2 is passed in the parameter, set PTP accordingly
	ldd 2,x
	cpd #2
	bne L155
	.dbline 482
	ldab #2
	stab 0x258
	bra L156
L155:
	.dbline 483
; 	else if(rowid == 3) PTP = 0x04; //if row 3 is passed in the parameter, set PTP accordingly
	ldd 2,x
	cpd #3
	bne L157
	.dbline 483
	ldab #4
	stab 0x258
	bra L158
L157:
	.dbline 484
; 	else if(rowid == 4) PTP = 0x08; //if row 4 is passed in the parameter, set PTP accordingly
	ldd 2,x
	cpd #4
	bne L159
	.dbline 484
	ldab #8
	stab 0x258
L159:
L158:
L156:
L154:
	.dbline 487
; 	
; 	//set U7_EN low
; 	PTM = PTM & 0xf7; 
	bclr 0x250,#0x8
	.dbline -2
	.dbline 488
; }
L152:
	tfr x,s
	pulx
	leas 2,sp
	.dbline 0 ; func end
	rts
	.dbsym l rowid 2 I
	.dbend
	.dbfunc e printLCD _printLCD fV
;              c -> 3,x
_printLCD::
	pshd
	pshx
	tfr s,x
	.dbline -1
	.dbline 495
; 
; /*
; Print the character on the board's LCD screen and increase 
; the cursor to the next position.
; *** This has been changed from question one. ***
; */
; void printLCD(char c) {
	.dbline 496
; 	 LCD_display(c); //load the character
	ldab 3,x
	clra
	jsr _LCD_display
	.dbline 497
; 	 LCD_instruction(0x06); //write character and shift to next position
	ldd #6
	jsr _LCD_instruction
	.dbline -2
	.dbline 498
; }
L161:
	tfr x,s
	pulx
	leas 2,sp
	.dbline 0 ; func end
	rts
	.dbsym l c 2 I
	.dbsym l c 3 c
	.dbend
	.dbfunc e printLCDSame _printLCDSame fV
;              c -> 3,x
_printLCDSame::
	pshd
	pshx
	tfr s,x
	.dbline -1
	.dbline 501
; 
; void printLCDSame(char c) 
; {
	.dbline 502
; 	 LCD_display(c); //load the character
	ldab 3,x
	clra
	jsr _LCD_display
	.dbline 503
; 	 LCD_instruction(0x8C); //write character and shift to next position
	ldd #140
	jsr _LCD_instruction
	.dbline -2
	.dbline 504
; }
L162:
	tfr x,s
	pulx
	leas 2,sp
	.dbline 0 ; func end
	rts
	.dbsym l c 2 I
	.dbsym l c 3 c
	.dbend
	.dbfunc e checkKey _checkKey fc
;           col4 -> -5,x
;           col3 -> -4,x
;           col2 -> -3,x
;           col1 -> -2,x
;              a -> -1,x
;              i -> 2,x
_checkKey::
	pshd
	pshx
	tfr s,x
	leas -6,sp
	.dbline -1
	.dbline 512
; 
; /*
; This checks the appropriate key on the board's keypad to see if it was pressed.
; It does this only once, so it must be run in a loop [see function waiting() above].
; Once it determines that a key has been pressed it runs the appropriate action
; and returns the character which was pressed.
; */
; char checkKey(int i) {
	.dbline 513
; 	 byte a = PTH & 0xf0;   //activates the columns in the keypad. (needed when polling the keys in a row) 
	; vol
	ldab 0x260
	clra
	anda #0
	andb #240
	stab -1,x
	.dbline 516
; 	 
; 	 //represent the columns of the array of keys (column 1 to 4)
; 	 byte col1 = 0x10;  //column 1
	ldab #16
	stab -2,x
	.dbline 517
; 	 byte col2 = 0x20; //column 2
	ldab #32
	stab -3,x
	.dbline 518
; 	 byte col3 = 0x40;	//column 3
	ldab #64
	stab -4,x
	.dbline 519
; 	 byte col4 = 0x80;  //column 4
	ldab #128
	stab -5,x
	.dbline 523
; 	 
; 	 
; 	 //row one of keys on keypad
; 	 if(i == 1){
	ldd 2,x
	cpd #1
	bne L164
	.dbline 523
	.dbline 525
; 	 	  //key '1' pressed
; 	 	  if (a==col1) {
	ldab -1,x
	cmpb -2,x
	bne L166
	.dbline 525
	.dbline 526
; 					   return '1';
	ldd #49
	lbra L163
L166:
	.dbline 530
; 			}
; 		  
; 		  //key '2'pressed
; 	 	  else if (a==col2)return '2';
	ldab -1,x
	cmpb -3,x
	bne L168
	.dbline 530
	ldd #50
	lbra L163
L168:
	.dbline 533
; 		  
; 		  //key '3' pressed 
; 	 	  else if (a==col3)return '3';
	ldab -1,x
	cmpb -4,x
	bne L170
	.dbline 533
	ldd #51
	lbra L163
L170:
	.dbline 536
; 		  
; 		  //key 'A' pressed
; 	 	  else if (a==col4){
	ldab -1,x
	cmpb -5,x
	bne L172
	.dbline 536
	.dbline 537
; 		  	  return 'A';
	ldd #65
	lbra L163
L172:
	.dbline 539
; 			  }
; 		  else return 0x00;
	ldd #0
	lbra L163
L164:
	.dbline 543
; 	 }
; 	 
; 	 //row two of keys on keypad
; 	 else if(i == 2){
	ldd 2,x
	cpd #2
	bne L174
	.dbline 543
	.dbline 545
; 	 	  //key '4' pressed
; 	 	  if (a==col1)return '4';
	ldab -1,x
	cmpb -2,x
	bne L176
	.dbline 545
	ldd #52
	lbra L163
L176:
	.dbline 548
; 		  
; 		  //key '5' pressed
; 	 	  else if (a==col2)return '5';
	ldab -1,x
	cmpb -3,x
	bne L178
	.dbline 548
	ldd #53
	lbra L163
L178:
	.dbline 551
; 		 
; 		 //key '6' pressed
; 	 	  else if (a==col3)return '6';
	ldab -1,x
	cmpb -4,x
	bne L180
	.dbline 551
	ldd #54
	lbra L163
L180:
	.dbline 554
; 		  
; 		  //key 'B' pressed
; 	 	  else if (a==col4){
	ldab -1,x
	cmpb -5,x
	bne L182
	.dbline 554
	.dbline 556
; 		  	   		   
; 			   return 'B';
	ldd #66
	bra L163
L182:
	.dbline 558
; 		  }
; 		  else return 0x00;
	ldd #0
	bra L163
L174:
	.dbline 562
; 	 }
; 	 
; 	  //row three of keys on keypad
; 	 else if(i == 3){
	ldd 2,x
	cpd #3
	bne L184
	.dbline 562
	.dbline 565
; 	 	  
; 		  ///key '7' pressed
; 	 	  if (a==col1)return '7';
	ldab -1,x
	cmpb -2,x
	bne L186
	.dbline 565
	ldd #55
	bra L163
L186:
	.dbline 568
; 		  
; 		  //key '8' pressed
; 	 	  else if (a==col2)return '8';
	ldab -1,x
	cmpb -3,x
	bne L188
	.dbline 568
	ldd #56
	bra L163
L188:
	.dbline 571
; 		  
; 		  //key '9' pressed
; 	 	  else if (a==col3)return '9';
	ldab -1,x
	cmpb -4,x
	bne L190
	.dbline 571
	ldd #57
	bra L163
L190:
	.dbline 574
; 		  
; 		  //key 'C' pressed
; 	  	  else if (a==col4){
	ldab -1,x
	cmpb -5,x
	bne L192
	.dbline 574
	.dbline 576
; 		
; 			   return 'C';
	ldd #67
	bra L163
L192:
	.dbline 578
; 		  }
; 		  else return 0x00;
	ldd #0
	bra L163
L184:
	.dbline 582
; 	 }
; 	 
; 	 //row four of keys on keypad
; 	 else if(i == 4){
	ldd 2,x
	cpd #4
	bne L194
	.dbline 582
	.dbline 584
; 	  	  //letter E
; 	  	  if (a==col1) return 'E';
	ldab -1,x
	cmpb -2,x
	bne L196
	.dbline 584
	ldd #69
	bra L163
L196:
	.dbline 586
; 		  //letter 0
; 	 	  else if (a==col2) return '0';
	ldab -1,x
	cmpb -3,x
	bne L198
	.dbline 586
	ldd #48
	bra L163
L198:
	.dbline 588
; 		  //letter F
; 	 	  else if (a==col3){   
	ldab -1,x
	cmpb -4,x
	bne L200
	.dbline 588
	.dbline 589
; 			   return 'F';
	ldd #70
	bra L163
L200:
	.dbline 592
; 		  }
; 		  //letter D
; 	  	  else if (a==col4){
	ldab -1,x
	cmpb -5,x
	bne L202
	.dbline 592
	.dbline 594
; 		  	 
; 			   return 'D';
	ldd #68
	bra L163
L202:
	.dbline 596
; 		  }
; 		  else return 0x00;
	ldd #0
	bra L163
L194:
	.dbline 598
; 	 }
; 	 return 0x00;
	ldd #0
	.dbline -2
L163:
	tfr x,s
	pulx
	leas 2,sp
	.dbline 0 ; func end
	rts
	.dbsym l col4 -5 c
	.dbsym l col3 -4 c
	.dbsym l col2 -3 c
	.dbsym l col1 -2 c
	.dbsym l a -1 c
	.dbsym l i 2 I
	.dbend
	.dbfunc e main _main fV
_main::
	.dbline -1
	.dbline 606
; }
; 
; 
; 
; /*
; Runs the simulation of 3 seconds a couple times with different values. 
; */
; void main() {
	.dbline 608
; 	 
; 	 key = 0xFF;
	ldab #255
	stab _key
	.dbline 610
; 	 //for keypad
; 	 DDRP |= 0x0F; // bitset PP0-3 as outputs (rows)  
	bset 0x25a,#15
	.dbline 611
; 	 DDRH &= 0x0F; // bitclear PH4..7 as inputs (columns)
	bclr 0x262,#0xf0
	.dbline 612
; 	 PTP = 0x0F; // Set scan row(s)
	ldab #15
	stab 0x258
	.dbline 614
; 	 //for interrupts
; 	 PIFH = 0xFF; // Clear previous interrupt flags
	ldab #255
	stab 0x267
	.dbline 615
; 	 PPSH = 0xF0; // Rising Edge
	ldab #240
	stab 0x265
	.dbline 616
; 	 PERH = 0x00; // Disable pulldowns
	clr 0x264
	.dbline 617
; 	 PIEH |= 0xF0; // Local enable on columns inputs
	bset 0x266,#240
	.dbline 621
; 
; 	 
; 	 //dialMode();
; 	 idleMode();
	jsr _idleMode
	.dbline -2
	.dbline 623
; 	 
; }
L204:
	.dbline 0 ; func end
	rts
	.dbend
	.area memory(abs)
	.org 0x3f8c
	.dbfile M:\2003\Assignment_5\assign56\assign56_mobile.c
_interrupt_vectors::
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word _KISR
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word 63695
	.word _OC_TIMER
	.word 63695
	.word 63695
	.word 63687
	.word 63683
	.word 63691
	.word 63493
	.word 63497
	.word __start
	.dbsym e interrupt_vectors _interrupt_vectors A[116:58]pfV
	.area data
	.dbfile M:\2003\Assignment_5\assign56\assign56_mobile.c
	.area bss
	.dbfile M:\2003\Assignment_5\assign56\assign56_mobile.c
_key::
	.blkb 1
	.dbsym e key _key c
	.area text
	.dbfile M:\2003\Assignment_5\assign56\assign56_mobile.c
L119:
	.byte 37,'d,0
L115:
	.byte 46,46,46,0
L110:
	.byte 'D,'I,'A,'L,'I,'N,'G,32,0
L98:
	.byte 'D,'u,'r,'a,'t,'i,'o,'n,58,0
L97:
	.byte 32,'C,'A,'L,'L,32,'E,'N,'D,'E,'D,33,32,0
L81:
	.byte 32,'T,58,0
L80:
	.byte 'E,58,32,'E,'n,'d,32,'C,'a,'l,'l,0
L79:
	.byte 32,'O,'N,32,'C,'A,'L,'L,0
L74:
	.byte 'C,'a,'l,'l,32,'R,'e,'j,'e,'c,'t,'e,'d,0
L70:
	.byte 'D,58,'D,'e,'n,'y,44,32,'A,58,'A,'n,'s,'w,'e,'r
	.byte 0
L69:
	.byte 32,'I,'N,'C,'O,'M,'I,'N,'G,32,'C,'A,'L,'L,33,0
L49:
	.byte 'E,'n,'t,'e,'r,32,'p,'h,'o,'n,'e,32,35,0
L48:
	.byte 'D,'i,'a,'l,58,0
L38:
	.byte 'C,'a,'l,'l,32,'D,58,'o,'u,'t,32,'C,58,'i,'n,0
L37:
	.byte 60,60,'O,'S,'Y,'A,'N,32,'M,'O,'B,'I,'L,'E,62,62
	.byte 0
L35:
	.byte 'd,'o,'n,'e,32,'d,'i,'a,'l,'i,'n,'g,0
