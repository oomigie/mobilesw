/**

README
Traffic light:





FLAGS:
pedsWaiting
carsWaiting
AmbulanceHERE



CarsWindow: 10seconds  
 
[(check every 10 seconds for pedsWaiting) ]

MODES
1) Oncoming cars:
     state: -green light: on ; carsWaiting: off
	 
	 after every "CarsWindow" seconds check pedsWaiting
	 if(pedsWaiting) {
	     - wait/delay for 5 seconds 
		 - display amber light for 3seconds
		 - display RED light    
	 }
	      
	  

2) Pedestrian walking (short street so pedestrians have only 10 7-segment counts ) 
    state: -red_light: on; carsWaiting: on
	
	-  display white LED 
	after 10 7-segment counts, 
	-turn off white LED,
	-turn on GREEN LIGHT  
	
	
	
	

KEYPAD MAPPING:
C - car sensor
B - Pedestrian sensor
A - Ambulance sensor


LIGHTS MAPPING:
red: oncoming cars (STOP) 
white: pedestrian walking
green: oncoming cars (GO)
amber: always display amber lights seconds before red 

7SEGMENT: count down for pedestrian walking
    
**/

#include <stdio.h>
